function Assert(cond,msg)   % for older MATLAB without assert()
    if (exist('assert') == 5)
        if (nargin < 2)
            assert(cond);
        else
            assert(cond,msg);
        end
    elseif (~cond)
        if (nargin < 2)
            msg = 'Assertion failed';
        end
        error(msg);
    end
end