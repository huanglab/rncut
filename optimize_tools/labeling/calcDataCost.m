function dataCost = calcDataCost(Imgdat, models)
% calculate data cost

global options

trackLoc = models.trackLoc;
trackImage =  Imgdat(trackLoc(1,2):trackLoc(1,4),trackLoc(1,1):trackLoc(1,3),:);
[height, width, ~] = size(trackImage);
nPoints = height*width;
nModels = models.nModels;

backColor = zeros(models.nBacks, nPoints);
backLoc = zeros(models.nBacks, nPoints);
foreColor = zeros(nModels, nPoints);
foreLoc = zeros(nModels, nPoints);

cenPoint = models.cenPoint;
forePoints = zeros(height, width, 2);
[forePoints(:,:,1), forePoints(:,:,2)] = meshgrid(trackLoc(1,1)-cenPoint(1):trackLoc(1,3)-cenPoint(1), trackLoc(1,2)-cenPoint(2):trackLoc(1,4)-cenPoint(2));
backPoints = zeros(height, width, 2);
[backPoints(:,:,1), backPoints(:,:,2)] = meshgrid(trackLoc(1,1):trackLoc(1,3), trackLoc(1,2):trackLoc(1,4));       

% calculate the color bins    
c_bins = calcColorBins(trackImage);
% calculate the datacost of background models
for k = 1:models.nBacks
    backColor(k,:) = exp(-models.backColorModel(k, c_bins)*options.appFactor);
    backLoc(k,:) = exp(-calcGaussProb(backPoints, models.backLocModel.mu(k,:), models.backLocModel.sigma(k,:), height, width)*options.locFactor);
end
% calculate the datacost of foreground models
for k = 1:models.nModels
    foreColor(k,:) = exp(-models.foreColorModel(k, c_bins)*options.appFactor);            
    foreLoc(k,:) = exp(-calcGaussProb(forePoints, models.foreLocModel.mu(k,:), models.foreLocModel.sigma(k,:), height, width)*options.locFactor);
end

%% calculate the final datacost
backCost = min(options.wdcost_color*backColor + options.wdcost_loc*backLoc);
foreCost = options.wdcost_color*foreColor + options.wdcost_loc*foreLoc;
dataCost = int32(cat(1, backCost, foreCost));

%% display the energy of datacost
if(options.showEnergy)
    figure(2);
    subplot(2,3,1),imshow(vec2mat(min(backColor),width)),title('backColor exp');
    subplot(2,3,2),imshow(vec2mat(min(backLoc),width)),title('backLoc exp');
    subplot(2,3,3),imshow(vec2mat(backCost,width)),title('backCost exp');
    subplot(2,3,4),imshow(vec2mat(min(foreColor),width)),title('foreColor exp');
    subplot(2,3,5),imshow(vec2mat(min(foreLoc),width)),title('foreLoc exp');
    subplot(2,3,6),imshow(vec2mat(min(foreCost),width)),title('foreCost exp');
    figure(3),imshow(vec2mat(backCost-min(foreCost), width)),title('datacost diff exp');
end