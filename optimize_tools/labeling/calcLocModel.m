function GaussModel = calcLocModel(trackRegion, mask, label, cenPoint)
% calculate location Gaussian model

global options

if(nargin == 3)
    cenPoint = [0 0];
end

train_height = trackRegion(2)-trackRegion(1)+1;
train_width = trackRegion(4)-trackRegion(3)+1;
nSp = numel(label);

GaussModel.mu = zeros(nSp, 2);
GaussModel.sigma = zeros(nSp, 2);

% parameter mu of Gaussian model for location
loc = zeros(train_height, train_width, 2);
[loc(:,:,1), loc(:,:,2)] = meshgrid(trackRegion(3)-cenPoint(1):trackRegion(4)-cenPoint(1), trackRegion(1)-cenPoint(2):trackRegion(2)-cenPoint(2));

for i = 1:2
    temp = loc(:,:,i);
    for k = 1:nSp
        tempVal = double(temp(mask == label(k)));
        GaussModel.mu(k,i) = mean(tempVal(:));
        GaussModel.sigma(k,i) = min(options.maxSigma, std(tempVal(:),1)*0.75); % ruduce the variance
    end
end