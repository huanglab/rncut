function allProb = calcGaussProb(x, mu, sigma, height, width, searchSigma)
% calculate location cost

global options

if(nargin == 5)
    searchSigma = [0 0];
end

dims = size(mu,2);
allProb = [];
for k = 1:size(mu,1)
    fx = zeros(size(x,1),size(x,2),dims);
    sigma = sigma + eps;
    for i = 1:dims
        fx(:,:,i) = 1/(sqrt(2*pi)*(sigma(k,i)+searchSigma(i)))*exp(-(x(:,:,i)-mu(k,i)).^2/(2*(sigma(k,i)+searchSigma(i))^2));
        temp = fx(:,:,i);
        idx1 = abs(x(:,:,1)-mu(k,1)) > max(options.maxSigma, width*0.2) + searchSigma(1);    
        idx2 = abs(x(:,:,2)-mu(k,2)) > max(options.maxSigma, height*0.2) + searchSigma(2);             
        temp(idx1 | idx2) = 0;
        fx(:,:,i) = temp;
    end

    Prob = prod(fx, 3);
    Prob = Prob';
    Prob = Prob(:);

    %calculate the geometric mean
    Prob = Prob.^(1/dims);
    allProb = cat(1, allProb, Prob');
end