function [dataCost, newForeCost, oldForeCost] = calcPreDataCost(oldModels, newModels, trackImage, foreColorModel, foreLocModel, fore_label)

global options;

[height, width, ~] = size(trackImage);
trackLoc = oldModels.trackLoc;
nPoints = height*width;

c_bins = calcColorBins(trackImage);

cenPoint = oldModels.cenPoint;
forePoints = zeros(height, width, 2);
[forePoints(:,:,1), forePoints(:,:,2)] = meshgrid(trackLoc(1,1)-cenPoint(1):trackLoc(1,3)-cenPoint(1), trackLoc(1,2)-cenPoint(2):trackLoc(1,4)-cenPoint(2));
backPoints = zeros(height, width, 2);
[backPoints(:,:,1), backPoints(:,:,2)] = meshgrid(trackLoc(1,1):trackLoc(1,3), trackLoc(1,2):trackLoc(1,4));

% calculate the background datacost
backCost.backColor = zeros(newModels.nBacks, nPoints);    
backCost.backLoc = zeros(newModels.nBacks, nPoints);    
for k = 1:newModels.nBacks
    backCost.backColor(k,:) = exp(-newModels.backColorModel(k, c_bins)*options.appFactor);
    backCost.backLoc(k,:) = exp(-calcGaussProb(backPoints, newModels.backLocModel.mu(k,:), newModels.backLocModel.sigma(k,:), height, width)*options.locFactor);
end

% calculate the old foreground datacost
oldForeCost.foreColor = zeros(oldModels.nModels, nPoints);
oldForeCost.foreLoc = zeros(oldModels.nModels, nPoints);    
for k = 1:oldModels.nModels
    oldForeCost.foreColor(k,:) = exp(-oldModels.foreColorModel(k, c_bins)*options.appFactor);
    oldForeCost.foreLoc(k,:) = exp(-calcGaussProb(forePoints, oldModels.foreLocModel.mu(k,:), oldModels.foreLocModel.sigma(k,:), height, width)*options.locFactor);
end

% calculate the new foreground datacost
newForeCost.foreColor = oldForeCost.foreColor;
newForeCost.foreLoc = oldForeCost.foreLoc;  
id = 0;
for k = 1:oldModels.nModels
    if(ismember(k+1, fore_label))
        id = id + 1;
        newForeCost.foreColor(k,:) = exp(-foreColorModel(id, c_bins)*options.appFactor);
        newForeCost.foreLoc(k,:) = exp(-calcGaussProb(forePoints, foreLocModel.mu(id,:), foreLocModel.sigma(id,:), height, width)*options.locFactor);
    end
end

% calculate the background datacost
allBackCost = options.wdcost_color*backCost.backColor + options.wdcost_loc*backCost.backLoc;
bCost = min(allBackCost);

% calculate the foreground datacost
fCost = options.wdcost_color*oldForeCost.foreColor + options.wdcost_loc*oldForeCost.foreLoc;

% calculate the datacost
dataCost = int32(cat(1, bCost, fCost));