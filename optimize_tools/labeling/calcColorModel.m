function colorModel = calcColorModel(img, mask, label)
% calculate HSV color hostigram model

nModels = numel(label);
nHBins = 6;nSBins = 6;nVBins = 6;
colorModel = zeros(nHBins*nSBins*nVBins, nModels);  

% c_bins(i,j)  is the histogram value index of a pixel
img = double(rgb2hsi(img));
hd = min(fix((img(:,:,1)*nHBins)), nHBins-1);
sd = min(fix((img(:,:,2)*nSBins)), nSBins-1);
vd = min(fix((img(:,:,3)*nVBins)), nVBins-1);
c_bins = vd*nHBins*nSBins + sd*nHBins + hd + 1;
% calculate the color histogram
t = tabulate(mask(:));
sp_pixel_num = t(:,2);
for k = 1:nModels
    x = c_bins(mask == label(k));
    %% count freq
    x = sort(x);
    frx = diff(x);
    frx = frx(:);
    frx(end + 1) = 1; 
    frx = find(frx);
    frx = min(numel(x), frx(:));
    value = x(frx);
    frx = [0; frx]; 
    freq = diff(frx); 
    colorModel(value, k) =  freq/sp_pixel_num(k);
end
% normalization of the histogram
colorModel = colorModel ./ repmat(sum(colorModel,1), size(colorModel,1), 1);
colorModel = colorModel';