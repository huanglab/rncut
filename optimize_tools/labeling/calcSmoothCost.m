function Scost = calcSmoothCost(models)
% calculate the smooth term

nModels = models.nModels;
Scost = ones(nModels+1, nModels+1);
Scost = Scost - diag(diag(Scost));