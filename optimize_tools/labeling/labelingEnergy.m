function E = labelingEnergy(Imgdat, models_, labeling, matchCost, dataCost)

if(nargin ~= 5)
    Dcost = calcDataCost(Imgdat, models_);
else
    Dcost = dataCost;
end    

Lcost =  calcLabelCost(models_);

[nLabels, nPoints] = size(Dcost);
h = GCO_Create(nPoints, nLabels);

% input the data term
GCO_SetDataCost(h, (Dcost));
% input the label term
GCO_SetLabelCost(h, int32(Lcost));
% input the labeling
GCO_SetLabeling(h, labeling);
% calculate the energy of current labeling
E = GCO_ComputeEnergy(h);
E = E + matchCost*100;

GCO_Delete(h);