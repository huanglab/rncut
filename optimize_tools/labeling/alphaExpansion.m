function [E, labeling] = alphaExpansion(Dcost, Scost, Lcost, Neighbor)
% minimize Es(f) wrt. f by alpha expansion
% 
% Copyright (C) Dawei Du and Longyin Wen.
%
% The code may be used free of charge for non-commercial and
% educational purposes, the only requirement is that this text is
% preserved within the derivative work. For any other purpose you
% must contact the authors for permission. This code may not be
% redistributed without written permission from the authors.


% The gco code is available at
% http://vision.csd.uwo.ca/code/


% set up GCO structure
[nLabels, nPoints] = size(Dcost);

h = GCO_Create(nPoints,nLabels);
GCO_SetDataCost(h, int32(Dcost));
GCO_SetLabelCost(h, int32(Lcost));

if ~isempty(Scost) && any(any(Scost))
    GCO_SetSmoothCost(h,int32(Scost));
    GCO_SetNeighbors(h, Neighbor);
end

GCO_Expansion(h);
labeling = GCO_GetLabeling(h)';
[E D S L] = GCO_ComputeEnergy(h);
GCO_Delete(h);