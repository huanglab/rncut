function c_bins = calcColorBins(img)

nHBins = 6;nSBins = 6;nVBins = 6;

% c_bins(i,j) is the histogram value index of a pixel
img = double(rgb2hsi(img));
hd = min(fix((img(:,:,1)*nHBins)), nHBins-1);
sd = min(fix((img(:,:,2)*nSBins)), nSBins-1);
vd = min(fix((img(:,:,3)*nVBins)), nVBins-1);
c_bins = vd*nHBins*nSBins + sd*nHBins + hd + 1;
% matrix into vector
c_bins = c_bins';
c_bins = c_bins(:);