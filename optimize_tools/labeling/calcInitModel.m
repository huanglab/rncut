function [spModels, segMask, labeling_] = calcInitModel(Imgdat, Imgdat_next, targetPos, trackLoc, cenPoint)

global options
%% find the label of the superpixel models (including the bacground models)
if(options.useGT)
    if(options.objects > 1)
        database = [options.datasetPath 'GroundTruth\' options.title(1:end-2) '\' options.title(end) '\']; 
    else
        database = [options.datasetPath 'GroundTruth\' options.title '\'];
    end
    gt = dir([database options.gtFormat]);
    segMask = rgb2gray(imread([database getfield(gt, {1}, 'name')]));
    segMask = imresize(segMask, options.img_scale);
else
    if(options.objects > 1)
        database = [options.datasetPath 'Initialization\' options.title(1:end-2) '\' options.title(end) '\'];         
    else
        database = [options.datasetPath 'Initialization\' options.title '\'];
    end
    initBase = dir([database '*.png']);
    initImg = initBase(2).name;
    segMask = imread([database initImg]);
    segMask = im2bw(segMask);
    segMask = imresize(segMask, [size(Imgdat,1) size(Imgdat,2)]);
end

%% SLIC superpixel oversegmentation
trackImage = Imgdat(trackLoc(2,2):trackLoc(2,4),trackLoc(2,1):trackLoc(2,3),:);
initSeg = segMask(trackLoc(2,2):trackLoc(2,4),trackLoc(2,1):trackLoc(2,3));
[spLabels, nSp] = overSegment(trackImage);
foreMask = spLabels.*(initSeg ~= 0);
backMask = spLabels.*(initSeg == 0);

num = 0;
fore_label = []; back_label = [];
foreFlag = zeros(1, nSp);
for k = 1:nSp
    sp_area = sum(sum(spLabels == k));
    fore_area = sum(sum(foreMask == k));
    if(fore_area > 0.5*sp_area)
        fore_label = cat(2, fore_label, k);
        foreFlag(k) = 1;
    else
        back_label = cat(2, back_label, k);        
        % save the region of the background model
        backLabels = zeros(size(Imgdat,1),size(Imgdat,2));
        backLabels(trackLoc(2,2):trackLoc(2,4), trackLoc(2,1):trackLoc(2,3)) = spLabels == k;
        num = num + 1;
        spModels.mBackLabels{num} = backLabels; 
    end
end

% calculate the initial labeling
labeling_ = refineFirstLabel(trackImage, spLabels, initSeg, foreFlag, nSp);
foreMask = labeling_.*(initSeg ~= 0);
fore_label = setdiff(unique(labeling_), 1);

% calculate the optical flow in tracking Regions
trackRegion = [trackLoc(2,2),trackLoc(2,4),trackLoc(2,1),trackLoc(2,3)];
spModels.oflow = calcFlow(Imgdat, Imgdat_next, trackRegion);
spModels.cenPoint = cenPoint;

% location for ROI region
trackImage = Imgdat(trackRegion(1):trackRegion(2),trackRegion(3):trackRegion(4),:);
spModels.trackLoc = trackLoc;

% number of superpixel models
spModels.nModels = numel(fore_label);
spModels.nBacks = numel(back_label);

% calculate the forground models
spModels.foreColorModel = calcColorModel(trackImage, foreMask, fore_label);
spModels.foreLocModel = calcLocModel(trackRegion, foreMask, fore_label, cenPoint);

% find the neighbors of superpixel models
spModels.mNeighbors = findSpNeighbors(foreMask, spModels);

% calculate the background model
spModels.backColorModel = calcColorModel(trackImage, backMask, back_label);
spModels.backLocModel = calcLocModel(trackRegion, backMask, back_label);

% color flag for superpixel models
spModels.mColors = 1:spModels.nModels;

% area cost for each superpixel model
for k = 1:numel(fore_label)
    spModels.mAreas(k) = sum(sum(foreMask == fore_label(k)));
end

% profile cost for each superpixel model
for k = 1:numel(fore_label)
    temp = foreMask == fore_label(k);
    spModels.mRatios(k) = getRatio(temp, spModels.foreLocModel.mu(k,:));
end

% number of times for foreground model usage
spModels.cntModels = zeros(1, spModels.nModels);

% target position
spModels.targetPos = targetPos;

% size of images
[imgHeight, imgWidth, ~] = size(Imgdat);
spModels.imgHeight = imgHeight;
spModels.imgWidth = imgWidth;