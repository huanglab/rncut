function oflow = calcFlow(Imgdat, Imgdat_next, trackRegion)

img1 = Imgdat(trackRegion(1):trackRegion(2),trackRegion(3):trackRegion(4),:);
img2 = Imgdat_next(trackRegion(1):trackRegion(2),trackRegion(3):trackRegion(4),:);    
oflow = mex_LDOF(double(img1), double(img2)); 