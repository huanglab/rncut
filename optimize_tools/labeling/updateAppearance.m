function newModels = updateAppearance(curModels, defalultModels, outMask)

global options;

trackLoc = curModels.trackLoc;
newModels = curModels;
w = options.update_weight;

% background models update
curLocSet = curModels.backLocModel.mu;
distMat = zeros(defalultModels.nBacks, curModels.nBacks);
distFlag = zeros(defalultModels.nBacks, curModels.nBacks);
for k = 1:defalultModels.nBacks
    preLoc = defalultModels.backLocModel.mu(k,:);
    dist = (repmat(preLoc, [size(curLocSet,1), 1]) - curLocSet);
    dist = sqrt(dist(:,1).^2 + dist(:,2).^2);
    distMat(k,:) = dist;
    idx1 = dist == min(dist);
    idx2 = dist <= options.threUpdateDist;
    distFlag(k, idx1 & idx2) = 1;
end
for k = 1:curModels.nBacks
    dist = distMat(:,k);
    idx1 = dist == min(dist);
    idx2 = dist <= options.threUpdateDist;    
    distFlag(idx1 & idx2, k) = distFlag(idx1 & idx2, k) + 1;
end

for k = 1:defalultModels.nBacks
    % delete the background models beyond the tracking region
    xy = defalultModels.backLocModel.mu(k,:);
    if(xy(1) < trackLoc(2,3) && xy(1) > trackLoc(2,1) && xy(2) < trackLoc(2,4) && xy(2) > trackLoc(2,2))
        backMask = defalultModels.mBackLabels{k};
        interRegion = outMask & backMask;
        % delete the background models shelter from foreground mask
        if(sum(interRegion(:)) < 0.3*sum(backMask(:)))
            updated = find(distFlag(k,:) == 2, 1, 'first');
            % update similar background models 
            if(~isempty(updated))
                colorDist = pdist2(curModels.backColorModel(updated,:), defalultModels.backColorModel(k,:), 'chisq');
                if(colorDist <= 0.6)
                    newModels.backColorModel(updated,:) = w*curModels.backColorModel(updated,:) + (1-w)*defalultModels.backColorModel(k,:);
                end
            end
        end
    end
end

% foreground models update
for k = 1:defalultModels.nModels
    colorDist = pdist2(curModels.foreColorModel(k,:), defalultModels.foreColorModel(k,:), 'chisq');
    if(colorDist <= 0.4)    
        newModels.foreColorModel(k,:) = w*curModels.foreColorModel(k,:) + (1-w)*defalultModels.foreColorModel(k,:);
    end
end