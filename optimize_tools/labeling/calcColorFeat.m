function colorFeat = calcColorFeat(img, mask, label)
% calculate hsv histogram color feature

nFeat = numel(label);  
nHBins = 6;nSBins = 6;nVBins = 6;
colorFeat = zeros(nFeat, nHBins*nSBins*nVBins + nSBins*nVBins + nHBins);  

% c_bins(i,j) is the histogram value index of a pixel
img = double(rgb2hsi(img));
hd = min(fix((img(:,:,1)*nHBins)), nHBins-1)+1;
sd = min(fix((img(:,:,2)*nSBins)), nSBins-1)+1;
vd = min(fix((img(:,:,3)*nSBins)), nVBins-1)+1;
c_bins = vd*nHBins*nSBins + sd*nHBins + hd;

% calculate the color histogram
for k = 1:nFeat
    for i = 1:size(img,1)
        for j = 1:size(img,2)
            if(mask(i,j) == label(k))
                colorFeat(k, c_bins(i,j)) = colorFeat(k, c_bins(i,j))+1;
            end
        end
    end
    %% normalization of the histogram
    colorFeat(k,:) = colorFeat(k,:)/sum(colorFeat(k,:));    
end