function Lcost = calcLabelCost(newModels)
% calculate label cost

global options

nModels = newModels.nModels;

%% calculate model number cost
numLabelCost = options.numcost*ones(1, nModels); 

%% calculate model region area cost
meanthre = mean(newModels.mAreas);
areaLabelCost = abs(newModels.mAreas-meanthre); 
areaLabelCost = options.areacost*(areaLabelCost-min(areaLabelCost))/(max(areaLabelCost)-min(areaLabelCost));

%% calculate model shape ratio cost
ratioLabelCost = newModels.mRatios; 
ratioLabelCost = options.ratiocost*(ratioLabelCost-min(ratioLabelCost))/(max(ratioLabelCost)-min(ratioLabelCost));

Lcost = [0, numLabelCost+areaLabelCost+ratioLabelCost]; % zero cost for background model