function Neighbors = calcNeighbors(trackImage, models, nPoints)
% calculate neighbor similarity

global options

trackLoc = models.trackLoc;
height = trackLoc(1,4) - trackLoc(1,2) + 1;
width = trackLoc(1,3) - trackLoc(1,1) + 1;

% calculate the color smooth matrix
colorFeat = zeros(nPoints, 3);
for c = 1:3
    temp = trackImage(:,:,c);
    temp = temp';
    colorFeat(:,c) = temp(:);
end

% calculate the optical gradient smooth matrix
oFlow = models.oflow(trackLoc(1,2)-trackLoc(2,2)+1:trackLoc(1,4)-trackLoc(2,2)+1, trackLoc(1,1)-trackLoc(2,1)+1:trackLoc(1,3)-trackLoc(2,1)+1,:);
flowFeat = zeros(nPoints, 2);
for c = 1:2
    temp = oFlow(:, :, c);
    temp = temp';
    flowFeat(:,c) = temp(:);
end       
alpha = 0.05;

% build the 4-neighbor connections
Neighbors = sparse(nPoints, nPoints);
for i = 1:height
    for j = 1:width
        idx = (i-1)*width + j;
        if(j>1)
            distColor = pdist2(colorFeat(idx,:), colorFeat(idx-1,:));
            distFlow = pdist2(flowFeat(idx,:), flowFeat(idx-1,:));            
            Neighbors(idx,idx-1) = options.wscost_color*exp(-alpha*distColor) + options.wscost_flow*exp(-1*distFlow);
        end
        if(j<width)
            distColor = pdist2(colorFeat(idx,:), colorFeat(idx+1,:));
            distFlow = pdist2(flowFeat(idx,:), flowFeat(idx+1,:));                        
            Neighbors(idx,idx+1) = options.wscost_color*exp(-alpha*distColor) + options.wscost_flow*exp(-1*distFlow);
        end
        if(i>1)
            distColor = pdist2(colorFeat(idx,:), colorFeat(idx-width,:));
            distFlow = pdist2(flowFeat(idx,:), flowFeat(idx-width,:));                        
            Neighbors(idx,idx-width) = options.wscost_color*exp(-alpha*distColor) + options.wscost_flow*exp(-1*distFlow);
        end
        if(i<height)
            distColor = pdist2(colorFeat(idx,:), colorFeat(idx+width,:));
            distFlow = pdist2(flowFeat(idx,:), flowFeat(idx+width,:));                        
            Neighbors(idx,idx+width) = options.wscost_color*exp(-alpha*distColor) + options.wscost_flow*exp(-1*distFlow);
        end
    end
end

Neighbors = options.lambda_smooth*Neighbors;