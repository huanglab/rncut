function [dataCost, foreCost] = findDataCost(oldDataCost, oldForeCost, newForeCost, fore_label)

global options

foreCost = oldForeCost;
foreCost.foreColor(fore_label-1, :) = newForeCost.foreColor(fore_label-1, :);
foreCost.foreLoc(fore_label-1, :) = newForeCost.foreLoc(fore_label-1, :);

% calculate the foreground datacost
fCost = options.wdcost_color*foreCost.foreColor + options.wdcost_loc*foreCost.foreLoc;

% calculate the datacost
dataCost = int32(cat(1, oldDataCost(1, :), fCost));