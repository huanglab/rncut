

clear all;close all;clc;
addpath(genpath(fullfile('optimize_tools'))); 
addpath(genpath(fullfile('Ncut_shi'))); 
addpath(genpath(fullfile('dataformat'))); 
stmat = ‘d:\mean_similarity.mat';           % prepare the .mat file
[ex1 ex2 ex3] = fileparts(stmat);
load(stmat);
directory_mask = ‘d:\brainGrayMatterMask.img';  % prepare the .img file                          
[brainGrayMatterMask,vox,dtype] = readanalyze(directory_mask); 
ClusterNum = 17;
cluster_result = RNcut(W,brainGrayMatterMask,ClusterNum);
write_ANALYZE(cluster_result,[ex1 filesep 'RNcut' '_' num2str(ClusterNum)  '_clusters.img'], size(brainGrayMatterMask),vox,1,16); 
