
clear all;
if 1          
dir_main = 'f:\Huang_lab\morris\Parcellation\data\adult\data';  
dir_fmri = 'f:\Huang_lab\morris\Parcellation\data\adult\data\fmri_data';
directory_mask = 'f:\Huang_lab\morris\Parcellation\data\adult\Mask\resample_mask_manual_gm_song_morris_46_55_46.img';     
[dataMask v d] = readanalyze(directory_mask); % 
end
 
cd(dir_fmri);
sm = 'similarity_matrix_group'; 
mkdir([dir_main filesep sm]);
cd(dir_fmri)
list = dir('sub*'); 
st = pwd;

%%
WW = 0;
volALL =[];
for i = 1:length(list)
subject_name =  list(i).name
stt =[st filesep subject_name filesep 'CovRegressed_4DVolume.nii']; 
[fmri_vol vox1 h1]=y_ReadRPI(stt);
dim =size(fmri_vol);
ind1 = find(dataMask>0); 
size(ind1)
fmri_s = reshape(fmri_vol,prod(dim(1:3)),dim(4));
data  = fmri_s(ind1,:);

%% normalization
tmp=data; % 
tmp=tmp-repmat(mean(tmp,2),[1,dim(4)]); % demean
tmp=tmp./repmat(sqrt(sum(tmp.^2,2)),[1,dim(4)]); % normalize to unit length
data = tmp;

%% 
W=corr(data',data'); 
W = abs(W);
W(isnan(W))=0;
W = W-diag(diag(W));  
W = single(W); 

%%
[t1 t2 t3] = fileparts(subject_name);
W = single(W);
WW = WW + W;
save([dir_main filesep sm filesep t2], 'W', '-v7.3'); % save the individual similarity matrix
disp([subject_name ' is Finished: ' num2str(i) ' / ' num2str(length(list))])
end
%%
clear W;
W = WW/length(list);
save([dir_main filesep sm filesep 'mean_similarity_' num2str(length(list))], 'W', '-v7.3');
save([dir_main filesep sm filesep 'mean_similarity_list_subject_' num2str(length(list))], 'list');
